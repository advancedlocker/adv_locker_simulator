using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Net;
using System.Net.Sockets;

namespace SocketServer
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>

    public class Thermo
    {
        public bool On;                     // The thermal system is enabled.
        public bool WithinControl;          // The chamber is within the Setpoint +/- Control Range
        public bool FanOn;                  // The state of the Fan system
        public Int32 FanSpeed;              // The speed of the fan if enabled (-1 = no fan installed)
        public Int32 ControlRange;          // The +/- deg c bound for the control algorythm
        public Int32 SetpointTemperature;   // The deg c setpoint temperature
        public Int32 CurrentTemperature;    // The deg c current temperature
    }

    public class Locker
    {
        public string Access_Code;          // A 3 to 10 digit access code. 
        public bool Assigned;               // The box is assigned
        public bool LockOpen;               // The lock is open (true) or locked (false)
        public bool DoorOpen;               // the door is open (true) or closed (false)
        public bool LEDOn;                  // The LED is on (true) or off  (false)
        public Int32 BatteryVoltage;        // The battery voltage within the locker
        public Thermo ThermalControl;       // The parameters regarding thermal control
    }

    public class Form1 : System.Windows.Forms.Form
	{
        public const int num_locks = 16;
        public const int SOCKET_TIMEOUT_MS = 100;

		public AsyncCallback pfnWorkerCallBack ;
		public  Socket m_socListener;
        public Socket m_socWorker;

        public System.Timers.Timer cmdTimer;
        public String RxCommand;
        public Locker[] Lockers = new Locker[num_locks];
        public string sessionkey = "123456789";

        private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtPortNo;
		private System.Windows.Forms.Button cmdListen;
		private System.Windows.Forms.TextBox txtDataRx;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox txtDataTx;
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //

            //Initialise all the lockers
            int i = 0;
            for(i = 0; i < num_locks; i++)
            {
                Lockers[i] = new Locker();
                Lockers[i].Access_Code = "";
                Lockers[i].Assigned = false;
                Lockers[i].LockOpen = false;
                Lockers[i].DoorOpen = false;
                Lockers[i].LEDOn = false;
                Lockers[i].ThermalControl = null;
            }

        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmdListen = new System.Windows.Forms.Button();
            this.txtPortNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDataRx = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDataTx = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmdListen);
            this.groupBox1.Controls.Add(this.txtPortNo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 48);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // cmdListen
            // 
            this.cmdListen.Location = new System.Drawing.Point(144, 16);
            this.cmdListen.Name = "cmdListen";
            this.cmdListen.Size = new System.Drawing.Size(104, 24);
            this.cmdListen.TabIndex = 2;
            this.cmdListen.Text = "Start Listening";
            this.cmdListen.Click += new System.EventHandler(this.cmdListen_Click);
            // 
            // txtPortNo
            // 
            this.txtPortNo.Location = new System.Drawing.Point(96, 16);
            this.txtPortNo.Name = "txtPortNo";
            this.txtPortNo.Size = new System.Drawing.Size(40, 20);
            this.txtPortNo.TabIndex = 1;
            this.txtPortNo.Text = "2051";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port Number:";
            // 
            // txtDataRx
            // 
            this.txtDataRx.Location = new System.Drawing.Point(8, 264);
            this.txtDataRx.Multiline = true;
            this.txtDataRx.Name = "txtDataRx";
            this.txtDataRx.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDataRx.Size = new System.Drawing.Size(272, 80);
            this.txtDataRx.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.txtDataTx);
            this.groupBox2.Location = new System.Drawing.Point(8, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(272, 152);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Send Data";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 120);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(232, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "Send";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtDataTx
            // 
            this.txtDataTx.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDataTx.Location = new System.Drawing.Point(8, 16);
            this.txtDataTx.Multiline = true;
            this.txtDataTx.Name = "txtDataTx";
            this.txtDataTx.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDataTx.Size = new System.Drawing.Size(240, 96);
            this.txtDataTx.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(0, 240);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(288, 112);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data Received";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(302, 446);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtDataRx);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Name = "Form1";
            this.Text = "Socket Server in C#";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void cmdListen_Click(object sender, System.EventArgs e)
		{
			try
			{
				//create the listening socket...
                if (m_socListener == null)
                {
				m_socListener = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);		
				IPEndPoint ipLocal = new IPEndPoint ( IPAddress.Any ,int.Parse(txtPortNo.Text));
				//bind to local IP Address...
				m_socListener.Bind( ipLocal );
                }
				//start listening...
				m_socListener.Listen (4);
                // create the call back for any client connections...
                m_socListener.ReceiveTimeout = 50;

                m_socListener.BeginAccept(new AsyncCallback ( OnClientConnect ),null);
				cmdListen.Enabled = false;

                cmdTimer = new System.Timers.Timer();
                cmdTimer.Elapsed += OnTimeoutEvt;
                cmdTimer.Interval = SOCKET_TIMEOUT_MS; // Use a 1 sec timewout to process the received command. 
			}
			catch(SocketException se)
			{
				MessageBox.Show ( se.Message );
			}
		}

        public bool IsConnected(Socket socket)
        {
            try
            {
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0);
            }
            catch (SocketException) { return false; }
            catch (Exception) { return false; }
        }

        public void OnTimeoutEvt(Object Src, System.Timers.ElapsedEventArgs e)
        {
            cmdTimer.Stop();

            if(!IsConnected(m_socWorker))
            {
                bool restart = false;

                if (m_socWorker.Connected == true)
                {
                    restart = true;
                    m_socWorker.Close();
                }

                if (m_socListener.Connected == true)
                {
                    restart = true;
                    m_socListener.Close();
                }
//                m_socWorker = null;
//                m_socListener = null;

                if(restart == true)
                    cmdListen_Click(this, null);

                cmdTimer.Interval = SOCKET_TIMEOUT_MS;
                return;
            }

            if ((RxCommand == null) || (RxCommand.Length == 0))
                return;

            string[] command = RxCommand.Split(',');
            string response = "";
            RxCommand = "";
            int len = command.Length; 
            if (command[0].Length > 0)
            {
                switch (command[0])
                {
                    case "GetSession":
                        response = "123456789";
                        break;
                    case "AssignLock":
                        {
                            if (command.Length != 6)
                                response = "AssignLock Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                    response = "AssignLock Received - incorrect key\n";

                                int j;
                                if (!Int32.TryParse(command[2], out j))
                                    response = "AssignLock Received - incorrect lock number\n";

                                if ((command[4].Length < 3) || (command[4].Length > 10))
                                    response = "AssignLock Received - incorrect verification key 3-10 chars\n";

                                if (command[5] != "12")
                                    response = "AssignLock Received - incorrect CRC\n";

                                response = "AssignLock-Success";
                                Lockers[j].Access_Code = command[4];
                                Lockers[j].Assigned = true;
                                Lockers[j].LockOpen = false;
                                Lockers[j].DoorOpen = false;
                                Lockers[j].LEDOn = true;
                            }
                        }
                        break;
                    case "UnassignLock":
                        {
                            if (command.Length != 4)
                                response = "UnassignLock Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                    response = "UnassignLock Received - incorrect key\n";

                                int j;
                                if (!Int32.TryParse(command[2], out j))
                                    response = "UnassignLock Received - incorrect lock number\n";

                                if (command[3] != "12")
                                    response = "UnassignLock Received - incorrect CRC\n";

                                response = "UnassignLock-Success";
                                Lockers[j].Access_Code = "";
                                Lockers[j].Assigned = false;
                                Lockers[j].LockOpen = true;
                                Lockers[j].DoorOpen = true;
                                Lockers[j].LEDOn = true;
                            }
                        }
                        break;
                    case "GetAvailableLocks":
                        {
                            if (command.Length != 3)
                                response = "GetAvailableLocks Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                { 
                                    response = "GetAvailableLocks Received - incorrect key\n";
                                    break;
                                }

                                if (command[2] != "12")
                                { 
                                    response = "GetAvailableLocks Received - incorrect CRC\n";
                                    break;
                                }

                                response = "GetAvailableLocks-";
                                int i = 0;
                                for(i = 0; i < num_locks; i++)
                                {
                                    if(Lockers[i].Assigned == false)
                                    {
                                        if (response.Length > 0)
                                            response += ",";
                                        response += i.ToString();
                                    }
                                }
                            }
                        }
                        break;
                    case "GetUsedLocks":
                        {
                            if (command.Length != 3)
                                response = "GetUsedLocks Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                {
                                    response = "GetUsedLocks Received - incorrect key\n";
                                    break;
                                }

                                if (command[2] != "12")
                                {
                                    response = "GetUsedLocks Received - incorrect CRC\n";
                                    break;
                                }

                                response = "GetUsedLocks-";
                                int i = 0;
                                for (i = 0; i < num_locks; i++)
                                {
                                    if (Lockers[i].Assigned == true)
                                    {
                                        if (response.Length > 0)
                                            response += ",";
                                        response += i.ToString();
                                    }
                                }
                            }
                        }
                        break;
                    case "ReserveLock":
                        response = "ReserveLock Received\n";
                        break;
                    case "OpenLock":
                        {
                            if (command.Length != 4)
                                response = "OpenLock Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                    response = "OpenLock Received - incorrect key\n";

                                int j;
                                if (!Int32.TryParse(command[2], out j))
                                    response = "OpenLock Received - incorrect lock number\n";

                                if (command[3] != "12")
                                    response = "OpenLock Received - incorrect CRC\n";

                                response = "OpenLock-Success";
                                Lockers[j].Assigned = false;
                                Lockers[j].LockOpen = true;
                                Lockers[j].DoorOpen = true;
                                Lockers[j].LEDOn = true;
                            }
                        }
                        break;
                    case "CloseLock":
                        {
                            if (command.Length != 4)
                                response = "CloseLock Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                    response = "CloseLock Received - incorrect key\n";

                                int j;
                                if (!Int32.TryParse(command[2], out j))
                                    response = "CloseLock Received - incorrect lock number\n";

                                if (command[3] != "12")
                                    response = "CloseLock Received - incorrect CRC\n";

                                response = "CloseLock-Success";
                                Lockers[j].Assigned = false;
                                Lockers[j].LockOpen = false;
                                Lockers[j].DoorOpen = false;
                                Lockers[j].LEDOn = false;
                            }
                        }
                        break;
                    case "GetLockStatus":
                        {
                            if (command.Length != 4)
                                response = "GetLockStatus Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                    response = "GetLockStatus Received - incorrect key\n";

                                int j;
                                if (!Int32.TryParse(command[2], out j))
                                    response = "GetLockStatus Received - incorrect lock number\n";

                                if (command[3] != "12")
                                    response = "GetLockStatus Received - incorrect CRC\n";

                                if(!Lockers[j].LockOpen)
                                    response = "GetLockStatus-State=Unlocked";
                                else
                                    response = "GetLockStatus-State=Locked";
                            }
                        }
                        break;
                    case "GetDoorStatus":
                        {
                            if (command.Length != 4)
                                response = "GetDoorStatus Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                    response = "GetDoorStatus Received - incorrect key\n";

                                int j;
                                if (!Int32.TryParse(command[2], out j))
                                    response = "GetDoorStatus Received - incorrect lock number\n";

                                if (command[3] != "12")
                                    response = "GetDoorStatus Received - incorrect CRC\n";

                                if (!Lockers[j].DoorOpen)
                                    response = "GetDoorStatus-State=Open";
                                else
                                    response = "GetDoorStatus-State=Closed";
                            }
                        }
                        break;
                    case "GetLedStatus":
                        {
                            if (command.Length != 4)
                                response = "GetLedStatus Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                    response = "GetLedStatus Received - incorrect key\n";

                                int j;
                                if (!Int32.TryParse(command[2], out j))
                                    response = "GetLedStatus Received - incorrect lock number\n";

                                if (command[3] != "12")
                                    response = "GetLedStatus Received - incorrect CRC\n";

                                if (!Lockers[j].LEDOn)
                                    response = "GetLedStatus-State=On";
                                else
                                    response = "GetLedStatus-State=Off";
                            }
                        }
                        break;

                    case "GetHWStatus":
                        {
                            if (command.Length != 4)
                                response = "GetHWStatus Received - incorrect data\n";
                            else
                            {
                                if (command[1] != sessionkey)
                                    response = "GetHWStatus Received - incorrect key\n";

                                int j;
                                if (!Int32.TryParse(command[2], out j))
                                    response = "GetHWStatus Received - incorrect lock number\n";

                                if (command[3] != "12")
                                    response = "GetHWStatus Received - incorrect CRC\n";

                                response = "GetHWStatus Received";
                            }
                        }
                        break;

                    default:
                        response = "Unknown Command : " + command[0] + "\n";
                        break;
                }

                byte[] byData = System.Text.Encoding.ASCII.GetBytes(response);

                m_socWorker.Send(byData);
            }
        }

        public void OnClientConnect(IAsyncResult asyn)
        {
            try
			{
				m_socWorker = m_socListener.EndAccept (asyn);
				 
				WaitForData(m_socWorker);
			}
			catch(ObjectDisposedException)
			{
				System.Diagnostics.Debugger.Log(0,"1","\n OnClientConnection: Socket has been closed\n");
//                Console.WriteLine(e.Message.ToString());
			}
			catch(SocketException se)
			{
//                MessageBox.Show(se.Message);
                Console.WriteLine(se.Message.ToString());
            }
            catch (Exception e)
            {
//                MessageBox.Show(e.Message);
                Console.WriteLine(e.Message.ToString());
			}
			
		}
		public class CSocketPacket
		{
			public System.Net.Sockets.Socket thisSocket;
			public byte[] dataBuffer = new byte[1];
		}

		public void WaitForData(System.Net.Sockets.Socket soc)
		{
			try
			{
				if  ( pfnWorkerCallBack == null ) 
				{
					pfnWorkerCallBack = new AsyncCallback (OnDataReceived);
				}

                if (soc.Connected)
                {
				CSocketPacket theSocPkt = new CSocketPacket ();
				theSocPkt.thisSocket = soc;

				// now start to listen for any data...
				soc .BeginReceive (theSocPkt.dataBuffer ,0,theSocPkt.dataBuffer.Length ,SocketFlags.None,pfnWorkerCallBack,theSocPkt);
                    cmdTimer.Start();       // Restart the 1sec timeout;
                }
			}
			catch(SocketException se)
			{
 //               MessageBox.Show(se.Message);
                Console.WriteLine(se.Message.ToString());
            }
            catch (Exception e)
            {
//                MessageBox.Show(e.Message);
                Console.WriteLine(e.Message.ToString());
			}

		}

		public  void OnDataReceived(IAsyncResult asyn)
		{
			try
			{
				CSocketPacket theSockId = (CSocketPacket)asyn.AsyncState ;
				//end receive...
				int iRx  = 0 ;
				iRx = theSockId.thisSocket.EndReceive (asyn);
//                char[] chars = new char[iRx + 1];
                char[] chars = new char[iRx];
                System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
				int charLen = d.GetChars(theSockId.dataBuffer, 0, iRx, chars, 0);
				System.String szData = new System.String(chars);
                RxCommand = RxCommand + szData;
                WaitForData(m_socWorker );
			}
			catch (ObjectDisposedException )
			{
				System.Diagnostics.Debugger.Log(0,"1","\nOnDataReceived: Socket has been closed\n");
			}
			catch(SocketException se)
			{
//                MessageBox.Show(se.Message);
                Console.WriteLine(se.Message.ToString());
			}
            catch (Exception e)
            {
//                MessageBox.Show(e.Message);
                Console.WriteLine(e.Message.ToString());
            }

		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				Object objData = txtDataTx.Text;
				byte[] byData = System.Text.Encoding.ASCII.GetBytes(objData.ToString ());
				m_socWorker.Send (byData);
			}
			catch(SocketException se)
			{
				MessageBox.Show (se.Message );
			}
		}

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
